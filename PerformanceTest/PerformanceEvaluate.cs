﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NBench;
using NBench.Util;
using TaskManager.BusinessLayer;
using TaskManager.Entity;

namespace PerformanceTest
{
    public class PerformanceEvaluate
    {
        TaskManagerBusiness obj = null;
        public PerformanceEvaluate()
        {
            obj = new TaskManagerBusiness();
        }

        [PerfBenchmark(Description = "Test to ensure that a minimal throughput test can be rapidly executed.",
                     NumberOfIterations = 1, RunMode = RunMode.Throughput,RunTimeMilliseconds = 15, TestMode = TestMode.Test)]
        [CounterThroughputAssertion("TestCounter", MustBe.LessThan, 10000000.0d)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThanOrEqualTo, 70000000)]
        [GcTotalAssertion(GcMetric.TotalCollections, GcGeneration.Gen2, MustBe.LessThanOrEqualTo, 4.0d)]
        public void BenchmarkAddUpdate()
        {
            Task task = new Task();
            task.TaskName = "Test444";
            task.ParentId = 1;
            task.StartDate = DateTime.Now;
            task.EndDate = DateTime.Now;
            task.Status = true;
            task.Priority = 12;
            
            obj.AddTask(task);
            obj.UpdateTask(task);
           // obj.EndTask(task);
        }

        [PerfBenchmark(Description = "Test to ensure that a minimal throughput test can be rapidly executed.",
                   NumberOfIterations = 1, RunMode = RunMode.Throughput,RunTimeMilliseconds = 15, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThanOrEqualTo, 70000000)]
        [GcTotalAssertion(GcMetric.TotalCollections, GcGeneration.Gen2, MustBe.LessThanOrEqualTo, 4.0d)]
        public void BenchmarkView()
        {   
            obj.GetAllTask();
        }

        [PerfBenchmark(Description = "Test to ensure that a minimal throughput test can be rapidly executed.",
          NumberOfIterations = 1, RunMode = RunMode.Throughput, RunTimeMilliseconds = 15, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThanOrEqualTo, 70000000)]
        [GcTotalAssertion(GcMetric.TotalCollections, GcGeneration.Gen2, MustBe.LessThanOrEqualTo, 4.0d)]
        public void BenchmarkGetByTaskId()
        {   
            obj.GetTaskById(1);
        }

        [PerfCleanup]
        public void Cleanup()
        {
            // does nothing
        }
    }
}
