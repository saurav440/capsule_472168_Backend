﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using TaskManager.Api.CustomFilter;
using TaskManager.Api.Models;
using TaskManager.BusinessLayer;
using TaskManager.Entity;

namespace TaskManager.Api.Controllers
{
    public class TaskManagerController : ApiController
    {
        TaskManagerBusiness businessObj = new TaskManagerBusiness();

        // GET: api/TaskManager
        [Route("api/GetAll")]
        [CustomeExceptionFilter]
        public List<TaskDetail> Get()
        {
            var response = businessObj.GetAllTask();
            return BuildTaskList(response);
        }

        // GET: api/TaskManager/5
        [Route("api/GetTask/{id}")]
        [HttpGet]
        [CustomeExceptionFilter]
        public IHttpActionResult Get(int id)
        {
            Task task = businessObj.GetTaskById(id);
            if(task == null)
            {
                return NotFound();
            }
            return Ok(task);
        }

        // POST: api/TaskManager
        [Route("api/AddTask")]
        [HttpPost]
        [CustomeExceptionFilter]
        public IHttpActionResult Post(Task task)
        {
            task.Status = true;
            businessObj.AddTask(task);
            return Ok();
        }

        // PUT: api/TaskManager/5
        [HttpPut]
        [Route("api/Update")]
        [CustomeExceptionFilter]
        public IHttpActionResult Put(Task task)
        {
            businessObj.UpdateTask(task);

            return Content(HttpStatusCode.Accepted,task);
        }

        [HttpPut]
        [Route("api/EndTask")]
        [CustomeExceptionFilter]
        public IHttpActionResult EndTask(Task task)
        {
           var response =  businessObj.EndTask(task.TaskId);
            if(response.Count == 0)
            {
                return NotFound();
            }
           
            return Ok(BuildTaskList(response));
        }
        // DELETE: api/TaskManager/5
        [Route("api/DeleteTask/{id}")]
        [HttpDelete]
        [CustomeExceptionFilter]
        public IHttpActionResult Delete(int id)
        {
            if (id <= 0)
                return BadRequest("Not a valid order number");

            var response =  businessObj.DeleteTask(id);
            if (response.Count == 0)
            {
                return NotFound();
            }
            //return Ok(BuildTaskList(response));
            return Content(HttpStatusCode.OK, BuildTaskList(response));

        }

        [Route("api/GetByTaskName/{taskname}")]
        [HttpGet]
        public Task GetByTaskName(string TaskName)
        {
            TaskManagerBusiness obj = new TaskManagerBusiness();
            return obj.GetByTaskName(TaskName);

        }

        private List<TaskDetail> BuildTaskList(List<Task> taskResp)
        {
            List<TaskDetail> tasks = new List<TaskDetail>();

            try
            {
                foreach (var item in taskResp)
                {
                    TaskDetail taskDetail = new TaskDetail();
                    taskDetail.TaskId = item.TaskId;
                    taskDetail.TaskName = item.TaskName;
                    taskDetail.ParentId = item.ParentId;
                    if (taskDetail.ParentId != null)
                    {
                         var resp = taskResp.FirstOrDefault(x => x.TaskId == item.ParentId);
                        taskDetail.ParentTask = resp != null ? resp.TaskName : null;
                    }
                    taskDetail.StartDate = item.StartDate;
                    taskDetail.EndDate = item.EndDate;
                    taskDetail.Priority = item.Priority;
                    taskDetail.Status = item.Status;

                    tasks.Add(taskDetail);
                }
            }
            catch(Exception ex)
            {
                Logger.WriteLog(ex.Message);
            }
            
            return tasks;
        }
    }
}
