﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using Newtonsoft.Json;
using NUnit.Framework;
using TaskManager.Api.Controllers;
using TaskManager.Api.Models;
using TaskManager.Entity;

namespace TaskManager.Api.Tests
{
    [TestFixture]
    public class TestWebApi
    {
        TaskManagerController _taskObj;
        int taskId = 0;
       
        [SetUp]
        public void Setup()
        {
            _taskObj = new TaskManagerController();
        }

        [Test]
        [Order(1)]
        public void TestWebApiGetAll()
        {   
            List<TaskDetail> tasks = _taskObj.Get();
            Assert.Greater(tasks.Count(), 0);
        }
        [Test]
        [Order(2)]
        public void TestWebApiAddTask()
        {
            // Arrange
            Task item = new Task();
            item.TaskName = "TaskApiTest1";
            item.ParentId = 1;
            item.Priority = 15;
            item.StartDate = DateTime.Now;
            item.EndDate = DateTime.Now;
            item.Status = true;

            //Act
            IHttpActionResult actionResult = _taskObj.Post(item);
            Task test = _taskObj.GetByTaskName("TaskApiTest1");
            taskId = test.TaskId;

            //Assert
            Assert.IsInstanceOf(typeof(OkResult), actionResult);
            // Assert.AreEqual("TaskTest2", test.TaskName);
        }
        [Test]
        [Order(3)]
        public void TestWebAPIGetByTaskId()
        { 
            IHttpActionResult actionResult = _taskObj.Get(taskId);
            var contentResult = actionResult as OkNegotiatedContentResult<Task>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(taskId, contentResult.Content.TaskId);
        }

        [Test]
        [Order(4)]
        public void TestGetByIdReturnsNotFound()
        {
            // Act
            IHttpActionResult actionResult = _taskObj.Get(10005);

            // Assert
            Assert.IsInstanceOf(typeof(NotFoundResult), actionResult);
        }
        [Test]
        [Order(5)]
        public void TestWebApiUpdateTask()
        {
            Task item = new Task();
            item.TaskId = taskId;
            item.TaskName = "TaskApiTest1";
            item.Priority = 10;
            item.ParentId = 1;
            item.StartDate = DateTime.Now;
            item.EndDate = DateTime.Now.AddDays(10);
            item.Status = true;

            IHttpActionResult actionResult = _taskObj.Put(item);
            var contentResult = actionResult as NegotiatedContentResult<Task>;
                      
            // Assert
            Assert.IsNotNull(contentResult);
            Assert.AreEqual(HttpStatusCode.Accepted, contentResult.StatusCode);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(10, contentResult.Content.Priority);
        }
        [Test]
        [Order(6)]
        public void TestWebApiEndTask()
        {
            // Arrange
            Task item = new Task();
            item.TaskId = taskId;
            item.TaskName = "TaskApiTest1";
            item.Priority = 10;
            item.StartDate = DateTime.Now;
            item.EndDate = DateTime.Now;
            item.Status = false;
                                
            //Act
            IHttpActionResult actionResult = _taskObj.EndTask(item);
            Task itemAfterEnd = _taskObj.GetByTaskName("TaskApiTest1");

            //Assert
          //  Assert.IsInstanceOf(typeof(OkResult), actionResult);
            Assert.AreEqual(false, itemAfterEnd.Status);
        }
        [Test]
        [Order(7)]
        public void TestWebApiDelete()
        {
            //Act
            IHttpActionResult actionResult = _taskObj.Delete(taskId);
            var contentResult = actionResult as NegotiatedContentResult<List<TaskDetail>>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.AreEqual(HttpStatusCode.OK, contentResult.StatusCode);
            Assert.IsNotNull(contentResult.Content);
        }
        [Test]
        [Order(8)]
        public void TestDeleteBadRequest()
        {
            //Act
            IHttpActionResult actionResult = _taskObj.Delete(0);

            //Assert
            Assert.IsInstanceOf(typeof(BadRequestErrorMessageResult), actionResult);
        }
    }
}
