﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TaskManager.Entity
{
    [Table("Task")]
    public class Task
    {
        [Key]
        public int TaskId { get; set; }
      //  [ForeignKey("TaskId")]
        public int? ParentId { get; set; }
        [Column(TypeName ="varchar"), StringLength(100)]
        [Required]
        public string TaskName { get; set; }
        [Required]
        [Column(TypeName = "Date")]
        public DateTime StartDate { get; set; }
        [Required]
        [Column(TypeName = "Date")]
        public DateTime EndDate { get; set; }
        [Required]
        public int Priority { get; set; }
        [Required]
        public bool Status { get; set; }

       // public Task Tasks { get; set; }  //Navigation
    }
}
