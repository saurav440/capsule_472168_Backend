﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using TaskManager.BusinessLayer;
using TaskManager.Entity;

namespace TaskManager.BussinessLayer.Test
{
    [TestFixture]
    public class TestBusiness
    {
        TaskManagerBusiness _taskObj;
        int taskId = 0;

        [SetUp]
        public void Setup()
        {
            _taskObj = new TaskManagerBusiness();
        }

        [Test]
        [Order(1)]
        public void TestGetAll()
        {  
            int actual = _taskObj.GetAllTask().Count;
            Assert.Greater(actual, 0);
        }
        [Test]
        [Order(2)]
        public void TestAddTask()
        {
            Task item = new Task();
            item.TaskName = "TestBusiness1";
            item.ParentId = 1;
            item.Priority = 15;
            item.StartDate = DateTime.Now;
            item.EndDate = DateTime.Now;
            item.Status = true;
            _taskObj.AddTask(item);

            Task test = _taskObj.GetByTaskName("TestBusiness1");
            taskId = test.TaskId;
            Assert.AreEqual("TestBusiness1", test.TaskName);
        }
        [Test]
        [Order(3)]
        public void TestGetByTaskId()
        {  
            Task item = _taskObj.GetTaskById(taskId);
            Assert.AreEqual(taskId, item.TaskId);
        }
       
        [Test]
        [Order(4)]
        public void TestUpdateTask()
        {   
            Task item = new Task();
            item.TaskId = taskId;
            item.TaskName = "TestBusiness1";
            item.Priority = 20;
            item.StartDate = DateTime.Now;
            item.EndDate = DateTime.Now.AddDays(10);
            item.Status = true;

            _taskObj.UpdateTask(item);
            Task itemafterupdate = _taskObj.GetByTaskName("TestBusiness1");
            Assert.AreEqual(20, itemafterupdate.Priority);

        }
        [Test]
        [Order(5)]
        public void TestEndTask()
        {   
            _taskObj.EndTask(taskId);

            Task itemafterupdate = _taskObj.GetByTaskName("TestBusiness1");
            Assert.AreEqual(false, itemafterupdate.Status);
        }
        [Test]
        [Order(6)]
        public void TestDeleteTask()
        {
            _taskObj.DeleteTask(taskId);

            Task item = _taskObj.GetTaskById(taskId);
            Assert.AreEqual(null, item);
        }
    }
}
